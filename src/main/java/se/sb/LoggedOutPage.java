package se.sb;

import org.apache.wicket.Page;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created:
 * Date: 2013-05-22
 * Time: 11:13
 */
public class LoggedOutPage extends WebPage {
    private static final Logger log = LoggerFactory.getLogger(LoggedOutPage.class);

    public LoggedOutPage() {
        add(new Label("isSessionValid", "Remote User: " + this.getSession().getId()));
//        HttpSession session = ((HttpServletRequest) getRequest().getContainerRequest()).getSession(false);
//        if (session != null) {
//            log.info("Session:<{}>", session.getId());
            log.info("Session:<{}>", getSession().getId());
//        log.info("isSessionInvalidated:{}", getWebSession().isSessionInvalidated());
//            if (session != null) {
//                session.invalidate();
                this.getSession().invalidate();
                log.info("Session invalidated:{}", this.getSession().isSessionInvalidated());
//                getSession().invalidateNow();
                //getWebSession().invalidateNow();
                //          log.info("isSessionInvalidated:{}", getWebSession().isSessionInvalidated());
//            }
//            log.info("Session:<{}>", session.getId());
//        }
    }

}

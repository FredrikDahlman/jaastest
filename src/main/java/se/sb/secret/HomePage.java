package se.sb.secret;

import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.pages.RedirectPage;
import org.apache.wicket.request.Request;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.WebPage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.sb.LoggedOutPage;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class HomePage extends WebPage {
	private static final long serialVersionUID = 1L;
    private static final Logger log = LoggerFactory.getLogger(HomePage.class);

    public HomePage(final PageParameters parameters) {
		super(parameters);
        String remoteUser;
        String principal;
        HttpServletRequest containerRequest;

        try{
            containerRequest = (HttpServletRequest) getRequest().getContainerRequest();
            remoteUser = containerRequest.getRemoteUser();
            principal = containerRequest.getUserPrincipal().getName();
        } catch (NullPointerException e){
            log.info("no request.." + e.getMessage());
            remoteUser = "no info";
            principal = "no info";
        }

        add(new Label("remoteUserLabel","Remote User: " + remoteUser));
        add(new Label("principalLabel", "Principal: " + principal));
		add(new Label("version", getApplication().getFrameworkSettings().getVersion()));

        Form logoutForm = new Form("logoutForm");
        logoutForm.add(new Button("logout"){
            @Override
            public void onSubmit() {
//                getSession().invalidateNow();
//                getWebSession().invalidateNow();
                HttpSession session = ((HttpServletRequest) getRequest().getContainerRequest()).getSession(false);
                log.info("Session:<{}>",session.getId());
//                session.invalidate();
//                getSession().invalidateNow();
//                getWebSession().invalidateNow();
//                getWebSession().invalidateNow();
                getRequestCycle().setResponsePage(new LoggedOutPage());
                }
                //                HttpSession session = ((HttpServletRequest) getRequest().getContainerRequest()).getSession(false);
//                log.info("Session:<{}>",session.getId());
//                log.info("isSessionInvalidated:{}", getWebSession().isSessionInvalidated());
//                if(session != null){
//                    session.invalidate();
//                    this.getSession().invalidate();
//                    log.info("Session invalidated");
//                    getSession().invalidateNow();
//                    getWebSession().invalidateNow();
//                    log.info("isSessionInvalidated:{}", getWebSession().isSessionInvalidated());
//                }
//                log.info("Session:<{}>",session.getId());
//            }
        });
        add(logoutForm);

    }
}
